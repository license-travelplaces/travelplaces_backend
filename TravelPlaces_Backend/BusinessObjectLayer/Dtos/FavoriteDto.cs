﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Dtos
{
    public class FavoriteDto
    {
        public int UserId { get; set; }
        public int TravelPlaceId { get; set; }
    }
}

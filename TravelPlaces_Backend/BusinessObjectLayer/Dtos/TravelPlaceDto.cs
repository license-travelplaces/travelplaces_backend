﻿using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Dtos
{
    public class TravelPlaceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Program { get; set; }
        public int Duration { get; set; }
        public string? ImageUrl { get; set; }
        public string? CityName { get; set; }
        public double? Distance { get; set; }
        public bool? IsFavorite { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
    }
}

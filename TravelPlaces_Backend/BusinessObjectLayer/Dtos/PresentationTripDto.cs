﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Dtos
{
    public class PresentationTripDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }

        public int NumberLocations { get; set; }
        public ICollection<string>? ImagesUrl { get; set; }

    }
}

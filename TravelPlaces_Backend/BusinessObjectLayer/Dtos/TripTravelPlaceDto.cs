﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Dtos
{
    public class TripTravelPlaceDto
    {
        public int Id { get; set; }
        public int? TravelPlaceId { get; set; }
        public string Name { get; set; }
        public string? HourOpen { get; set; }
        public string? HourClose { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public double Duration { get; set; }

    }
}

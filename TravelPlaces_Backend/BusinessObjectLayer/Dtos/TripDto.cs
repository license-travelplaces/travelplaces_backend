﻿using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Dtos
{
    public class TripDto
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string HourStart { get; set; }
        public string HourEnd { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public string? StartLocationAddress { get; set; }
        public string? StartLocationName { get; set; }
        public int UserId { get; set; }
        public bool isStartEnd { get; set; }
        public ICollection<EdgeDto> edges { get; set; }
        public ICollection<TripTravelPlaceDto> travelPlacesTrip { get; set; }

    }
}

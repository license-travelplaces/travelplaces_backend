﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Dtos
{
    public class EdgeDto
    {
        public int NodeA { get; set; }
        public int NodeB { get; set; }

        public double Distance { get; set; }

        public double DValue { get; set; }

        public double TValue { get; set; }

        public string DText { get; set; }

        public string TText { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Entities
{
    [Table("TravelPlaces")]
    public class TravelPlaceEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }

        public double Lat { get; set; }

        public double Long { get; set; }

        public int Duration { get; set; }

        public string Program { get; set; }

        [Required]
        [ForeignKey("City")]
        public int CityId { get; set; }

        public virtual CityEntity City { get; set; }

        public virtual ICollection<TravelPlaceImageEntity> Images { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Entities
{
    [Table("TravelPlaceImages")]
    public class TravelPlaceImageEntity
    {

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        [Required]
        [ForeignKey("TravelPlace")]
        public int TravelPlaceId { get; set; }

        public virtual TravelPlaceEntity TravelPlace { get; set; }

    }
}

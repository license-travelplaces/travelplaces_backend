﻿using FluentNHibernate.Conventions.Inspections;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Entities
{
    [Table("Edges")]
    public class EdgeEntity
    {

        [ForeignKey("Trip")]
        public int TripId { get; set; }

        [ForeignKey("TravelPlaceStart")]
        public int StartTravelPlaceId { get; set; }

        [ForeignKey("TravelPlaceEnd")]
        public int EndTravelPlaceId { get; set; }

        public double Time { get; set; }
        public virtual TripEntity Trip { get; set; }
        public virtual TravelPlaceEntity TravelPlaceStart { get; set; }
        public virtual TravelPlaceEntity TravelPlaceEnd { get; set; }
        public double DValue { get; set; }

        public double TValue { get; set; }

        public string DText { get; set; }

        public string TText { get; set; }

    }
}

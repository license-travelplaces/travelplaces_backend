﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Entities
{
    [Table("Favorites")]
    public class FavoriteEntity
    {
        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }

        [Required]
        [ForeignKey("TravelPlace")]
        public int TravelPlaceId { get; set; }
        public virtual UserEntity User { get; set; }
        public virtual TravelPlaceEntity TravelPlace { get; set; }

    }
}

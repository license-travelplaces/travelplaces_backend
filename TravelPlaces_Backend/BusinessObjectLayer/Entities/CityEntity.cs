﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Entities
{
    [Table("Cities")]
    public class CityEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public double? Lat { get; set; }
        
        public double? Long { get; set; }

        public string? ImageUrl { get; set; }

        [Required]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        public virtual CountryEntity Country { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Entities
{
    [Table("Trips")]
    public class TripEntity
    {

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public double LatStart { get; set; }

        [Required]
        public double LongStart { get; set; }

        public string? StartLocationAddress { get; set; }

        public string? StartLocationName { get; set; }

        [Required]
        public DateTime DateStart { get; set; }

        [Required]
        public string StartHour { get; set; }

        [Required]
        public string EndHour { get; set; }

        [Required]
        public bool IsStartEnd { get; set; }

        [Required]
        public double Duration { get; set; }

        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }

        public virtual UserEntity User { get; set; }

        public virtual ICollection<EdgeEntity> Edges { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Enums
{
    public enum  UserType
    {
        NormalUser = 0,
        AdminUser = 1,
    }
}

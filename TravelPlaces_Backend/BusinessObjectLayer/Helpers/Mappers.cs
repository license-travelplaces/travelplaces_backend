﻿using AutoMapper;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.Helpers
{
    public class Mappers: Profile
    {
        public Mappers()
        {
            CreateMap<TravelPlaceEntity, TravelPlaceDto>();
            CreateMap<UserEntity, UserDto>()
                .ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<UserDto, UserEntity>();
            CreateMap<FavoriteDto, FavoriteEntity>()
                .ForMember(x => x.User, opt => opt.Ignore())
                .ForMember(x => x.TravelPlace, opt => opt.Ignore());
            CreateMap<FavoriteEntity, FavoriteDto>();
        }
    }
}

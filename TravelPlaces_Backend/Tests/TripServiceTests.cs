﻿using AutoMapper;
using BusinessLogicLayer.Services.Trip;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class TripServiceTests
    {
        private readonly TripService tripService;
        private readonly Mock<ITravelPlaceRepository> mockTravelPlaceRepository = new Mock<ITravelPlaceRepository>();
        private readonly Mock<ITripRepository> mockTripRepository = new Mock<ITripRepository>();
        private readonly Mock<IMapper> mockMapper = new Mock<IMapper>();

        public TripServiceTests()
        {
            this.tripService = new TripService(mockTravelPlaceRepository.Object, mockMapper.Object, mockTripRepository.Object);
        }

        [Fact]
        public async Task CreateTrip_ShoudReturnTrue_WhenTripIsCreated()
        {
            var edges = new List<EdgeDto>();
            edges.Add(new EdgeDto
            {
                NodeA = 0,
                NodeB = 1,
                Distance = 100,
                DText = "100 m",
                DValue = 100,
                TText = "10 min",
                TValue = 10,
            });

            var travelPlaces = new List<TripTravelPlaceDto>();
            travelPlaces.Add(new TripTravelPlaceDto
            {
                Id = 0,
                Duration = 60,
                HourClose = "17:00",
                HourOpen = "09:00",
                Lat = 46.7716683,
                Long = 23.5866262,
                TravelPlaceId = 1,
                Name = "Location1",
            });
            travelPlaces.Add(new TripTravelPlaceDto
            {
                Id = 1,
                Duration = 60,
                HourClose = "17:00",
                HourOpen = "09:00",
                Lat = 46.7716024,
                Long = 23.5841182,
                TravelPlaceId = null,
                Name = "Location1",
            });

            var trip = new TripDto
            {
                Name ="Test",
                Date = DateTime.Now,
                HourEnd = "17:00",
                HourStart = "09:00",
                Lat = 46.7716024,
                Long = 23.5841182,
                StartLocationAddress = "Test",
                StartLocationName = "Test",
                isStartEnd = true,
                edges = edges,
                travelPlacesTrip = travelPlaces
            };

            var respons = await tripService.CreateTrip(trip);
            Assert.Equal(respons, true);

            trip.isStartEnd = false;
            respons = await tripService.CreateTrip(trip);
            Assert.Equal(respons, true);
        }

        [Fact]
        public async Task CreateTrip_ShoudReturnFalse_WhenTripIsNotCreated()
        {
            var edges = new List<EdgeDto>();
            edges.Add(new EdgeDto
            {
                NodeA = 0,
                NodeB = 1,
                Distance = 10000,
                DText = "100 m",
                DValue = 100,
                TText = "10 min",
                TValue = 10,
            });

            var travelPlaces = new List<TripTravelPlaceDto>();
            travelPlaces.Add(new TripTravelPlaceDto
            {
                Id = 0,
                Duration = 180,
                HourClose = "17:00",
                HourOpen = "09:00",
                Lat = 46.7716683,
                Long = 23.5866262,
                TravelPlaceId = 1,
                Name = "Location1",
            });
            travelPlaces.Add(new TripTravelPlaceDto
            {
                Id = 1,
                Duration = 60,
                HourClose = "17:00",
                HourOpen = "15:00",
                Lat = 46.7716024,
                Long = 23.5841182,
                TravelPlaceId = null,
                Name = "Location1",
            });

            var trip = new TripDto
            {
                Name = "Test",
                Date = DateTime.Now,
                HourEnd = "17:00",
                HourStart = "09:00",
                Lat = 46.7716024,
                Long = 23.5841182,
                StartLocationAddress = "Test",
                StartLocationName = "Test",
                isStartEnd = true,
                edges = edges,
                travelPlacesTrip = travelPlaces
            };

            var respons = await tripService.CreateTrip(trip);
            Assert.Equal(respons, false);
        }

        [Fact]
        public async Task GetTripByTripId_ShoudReturnTripById_WhenTripExist()
        {
            var tripId = 4;
            var edges = new List<EdgeDto>();
            edges.Add(new EdgeDto
            {
                NodeA = 0,
                NodeB = 1,
                Distance = 10000,
                DText = "100 m",
                DValue = 100,
                TText = "10 min",
                TValue = 10,
            });

            var travelPlaces = new List<TripTravelPlaceDto>();
            travelPlaces.Add(new TripTravelPlaceDto
            {
                Id = 0,
                Duration = 180,
                HourClose = "17:00",
                HourOpen = "09:00",
                Lat = 46.7716683,
                Long = 23.5866262,
                TravelPlaceId = 1,
                Name = "Location1",
            });
            travelPlaces.Add(new TripTravelPlaceDto
            {
                Id = 1,
                Duration = 60,
                HourClose = "17:00",
                HourOpen = "15:00",
                Lat = 46.7716024,
                Long = 23.5841182,
                TravelPlaceId = null,
                Name = "Location1",
            });

            var trip = new TripEntity
            {
                Id = 4,
                Name = "Test",
                DateStart = DateTime.Now,
                EndHour = "17:00",
                StartHour = "09:00",
                LatStart = 46.7716024,
                LongStart = 23.5841182,
                StartLocationAddress = "Test",
                StartLocationName = "Test",
                IsStartEnd = true,
                Duration = 200,
                UserId = 1,
            };

            mockTripRepository.Setup(x => x.GetTripByTripId(tripId))
              .ReturnsAsync(trip);

            var respons = await tripService.GetTripByTripId(tripId);
            Assert.Equal(respons.Id, tripId);
            Assert.Equal(respons.Duration, 200);
        }

        //[Fact]
        //public async Task GetTripsByUserId_ShoudReturnTripByUserId_WhenExist()
        //{
        //    var tripId = 4;
        //    var userId = 1;
        //    var edges = new List<EdgeEntity>();
        //    edges.Add(new EdgeEntity
        //    {
        //        StartTravelPlaceId = 1,
        //        EndTravelPlaceId = 2,
        //        Time = 100,
        //        DText = "100 m",
        //        DValue = 100,
        //        TText = "10 min",
        //        TValue = 10,
        //    });

        //    //var travelPlaces = new List<TripTravelPlaceDto>();
        //    //travelPlaces.Add(new TripTravelPlaceDto
        //    //{
        //    //    Id = 0,
        //    //    Duration = 180,
        //    //    HourClose = "17:00",
        //    //    HourOpen = "09:00",
        //    //    Lat = 46.7716683,
        //    //    Long = 23.5866262,
        //    //    TravelPlaceId = 1,
        //    //    Name = "Location1",
        //    //});
        //    //travelPlaces.Add(new TripTravelPlaceDto
        //    //{
        //    //    Id = 1,
        //    //    Duration = 60,
        //    //    HourClose = "17:00",
        //    //    HourOpen = "15:00",
        //    //    Lat = 46.7716024,
        //    //    Long = 23.5841182,
        //    //    TravelPlaceId = null,
        //    //    Name = "Location1",
        //    //});
        //    //travelPlaces.Add(new TripTravelPlaceDto
        //    //{
        //    //    Id = 2,
        //    //    Duration = 60,
        //    //    HourClose = "17:00",
        //    //    HourOpen = "15:00",
        //    //    Lat = 46.7716024,
        //    //    Long = 23.5841182,
        //    //    TravelPlaceId = null,
        //    //    Name = "Location1",
        //    //});

        //    var trip = new TripEntity
        //    {
        //        Id = 4,
        //        Name = "Test",
        //        DateStart = DateTime.Now,
        //        EndHour = "17:00",
        //        StartHour = "09:00",
        //        LatStart = 46.7716024,
        //        LongStart = 23.5841182,
        //        StartLocationAddress = "Test",
        //        StartLocationName = "Test",
        //        IsStartEnd = true,
        //        Duration = 200,
        //        UserId = 1,
        //        Edges = edges,
        //    };

        //    mockTripRepository.Setup(x => x.GetTripsByUserId(userId))
        //      .ReturnsAsync(new List<TripEntity>() { trip });
        //    mockTravelPlaceRepository.Setup(x => x.GetImagesByTravelPlacesIds(new List<int>() { 1, 2 }))
        //        .ReturnsAsync(new List<TravelPlaceEntity>());

        //    var respons = await tripService.GetTripByTripId(tripId);
        //    Assert.Equal(respons.Name, trip.Name);
        //}


    }
}

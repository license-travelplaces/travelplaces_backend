﻿namespace Tests
{
    internal class USerEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public int CountryId { get; set; }
        public string ImageUrl { get; set; }
    }
}
﻿using AutoMapper;
using BusinessLogicLayer.Services.Users;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class UserServiceTests
    {
        private readonly UserService userService;
        private readonly Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
        private readonly Mock<IMapper> mockMapper = new Mock<IMapper>();

        public UserServiceTests()
        {
            this.userService = new UserService(mockUserRepository.Object, mockMapper.Object);
        }

        [Fact]
        public async Task GetUserByUsernameAndPassword_ShoudReturnUser_WhenExistt()
        {
            var username = "Test";
            var password = "Test";

            var user = new UserEntity
            {
                Id = 4,
                Username = "Test",
                Password = "Test",
                Email ="Test",

            };
            mockUserRepository.Setup(x => x.GetUserByUsernameAndPassword(username,password))
                .ReturnsAsync(user);
            var respons = await userService.GetUserByUsernameAndPassword(username,password);
            Assert.Equal(respons.Id, user.Id);
            Assert.Equal(respons.Username, user.Username);
            Assert.Equal(respons.Password, null);
            Assert.Equal(respons.Email, user.Email);
        }
    }
}

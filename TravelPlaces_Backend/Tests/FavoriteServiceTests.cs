﻿using AutoMapper;
using BusinessLogicLayer.Services.Favorites;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class FavoriteServiceTests
    {
        private readonly FavoriteService cityService;
        private readonly Mock<IFavoriteRepository> mockFavoriteRepository = new Mock<IFavoriteRepository>();
        private readonly Mock<IMapper> mockMapper = new Mock<IMapper>();
        public FavoriteServiceTests()
        {
            this.cityService = new FavoriteService(mockFavoriteRepository.Object,mockMapper.Object);
        }


        [Fact]
        public async Task GetAllFavoriteByUserId_ShoudGetAllFavoritesByUserId_WhenHeHas()
        {
            var list = new List<TravelPlaceEntity>();
            var userId = 1;

            for (int i = 0; i < 3; i++)
            {
                list.Add(new TravelPlaceEntity
                {
                    Id = i,
                    Name = "Test",
                    Lat = 34.33,
                    Long = 44.4,
                    Description = "Test",
                    Duration = 60,
                    Program = "test.png",
                    CityId = 4,
                    City = new CityEntity
                    {
                        Id = i,
                        Name = "Bucuresti",
                        Lat = 34.33,
                        Long = 44.4,
                        CountryId = 1,
                        ImageUrl = "test.png"
                    },
                    Images = new List<TravelPlaceImageEntity>(),

                }) ;
            }

            mockFavoriteRepository.Setup(x => x.GetAllTravelPlacesByUserId(userId))
                .ReturnsAsync(list);

            var listFavorites = await cityService.GetAllFavoriteByUserId(userId);
            
            Assert.Equal(listFavorites.Count, list.Count);
            Assert.Equal(listFavorites[0].IsFavorite, true);
        }

        [Fact]
        public async Task GetAllFavoriteByUserId_ShoudGetNull_WhenHeDoesNotHave()
        {
            var list = new List<TravelPlaceEntity>();
            var userId = 1;

            mockFavoriteRepository.Setup(x => x.GetAllTravelPlacesByUserId(userId))
                .ReturnsAsync(() => null);

            var listFavorites = await cityService.GetAllFavoriteByUserId(userId);

            Assert.Equal(listFavorites, null);
        }
    }
}

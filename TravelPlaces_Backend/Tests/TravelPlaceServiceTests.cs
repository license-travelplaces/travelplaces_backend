﻿using AutoMapper;
using BusinessLogicLayer.Services.TravelPlaces;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class TravelPlaceServiceTests
    {
        private readonly TravelPlaceService travelPlaceService;
        private readonly Mock<ITravelPlaceRepository> mockTravelPlaceRepository = new Mock<ITravelPlaceRepository>();
        private readonly Mock<IFavoriteRepository> mockFavoriteRepository = new Mock<IFavoriteRepository>();
        private readonly Mock<IMapper> mockMapper = new Mock<IMapper>();
        public TravelPlaceServiceTests()
        {
            this.travelPlaceService = new TravelPlaceService(mockTravelPlaceRepository.Object, mockMapper.Object, mockFavoriteRepository.Object);
        }


        [Fact]
        public async Task GetAllTravelPlacesByCityId_ShoudReturnAllTravelPlacesByCityId()
        {
            var list = new List<TravelPlaceEntity>();
            var cityId = 4;

            for (int i = 0; i < 3; i++)
            {
                list.Add(new TravelPlaceEntity
                {
                    Id = i,
                    Name = "Test",
                    Lat = 34.33,
                    Long = 44.4,
                    Description = "Test",
                    Duration = 60,
                    Program = "test.png",
                    CityId = 4,
                    City = new CityEntity
                    {
                        Id = i,
                        Name = "Bucuresti",
                        Lat = 34.33,
                        Long = 44.4,
                        CountryId = 1,
                        ImageUrl = "test.png"
                    },
                    Images = new List<TravelPlaceImageEntity>(),

                });
            }

            mockTravelPlaceRepository.Setup(x => x.GetAllTravelPlacesByCityId(cityId))
                .ReturnsAsync(list);

            var listTravelPlaces = await travelPlaceService.GetAllTravelPlacesByCityId(cityId);

            Assert.Equal(listTravelPlaces.Count, list.Count);
            Assert.Equal(listTravelPlaces[0].Name, list[0].Name);
        }

        [Fact]
        public async Task GetAllTravelPlacesByCityId_ShoudReturnNull()
        {
            var list = new List<TravelPlaceEntity>();
            var cityId = 4;

            mockTravelPlaceRepository.Setup(x => x.GetAllTravelPlacesByCityId(cityId))
                 .ReturnsAsync(() => null);

            var listTravelPlaces = await travelPlaceService.GetAllTravelPlacesByCityId(cityId);

            Assert.Equal(listTravelPlaces, null);
        }

        [Fact]
        public async Task GetAllTravelPlacesByCityIdIfUserLogged_ShoudReturnAllTravelPlacesByCityIdAndUserId()
        {
            var list = new List<TravelPlaceEntity>();
            var listFavorites = new List<FavoriteEntity>();
            var cityId = 4;
            var userId = 1;

            for (int i = 0; i < 3; i++)
            {
                list.Add(new TravelPlaceEntity
                {
                    Id = i,
                    Name = "Test",
                    Lat = 34.33,
                    Long = 44.4,
                    Description = "Test",
                    Duration = 60,
                    Program = "test.png",
                    CityId = 4,
                    City = new CityEntity
                    {
                        Id = i,
                        Name = "Bucuresti",
                        Lat = 34.33,
                        Long = 44.4,
                        CountryId = 1,
                        ImageUrl = "test.png"
                    },
                    Images = new List<TravelPlaceImageEntity>(),

                });
            }

            for (int i = 0; i < 2; i++)
            {
                listFavorites.Add(new FavoriteEntity
                {
                    TravelPlaceId = i,
                    UserId = userId,
                });
            }

            mockTravelPlaceRepository.Setup(x => x.GetAllTravelPlacesByCityId(cityId))
                .ReturnsAsync(list);
            mockFavoriteRepository.Setup(x => x.GetAllFavoriteByUserId(userId))
              .ReturnsAsync(listFavorites);

            var listTravelPlaces = await travelPlaceService.GetAllTravelPlacesByCityIdIfUserLogged(cityId,userId);

            Assert.Equal(listTravelPlaces.Count, list.Count);
            Assert.Equal(listTravelPlaces[2].IsFavorite, false);
            Assert.True(listTravelPlaces[0].IsFavorite);
        }

        [Fact]
        public async Task GetTravelPlaceById_ShoudReturnTravelPlace_WhenExist()
        {
            var travelPlaceId = 1;

            var tarvelPlace = new TravelPlaceEntity
            {
                Id = 1,
                Name = "Test",
                Lat = 34.33,
                Long = 44.4,
                Description = "Test",
                Duration = 60,
                Program = "test.png",
                CityId = 4,
                City = new CityEntity
                {
                    Id = 1,
                    Name = "Bucuresti",
                    Lat = 34.33,
                    Long = 44.4,
                    CountryId = 1,
                    ImageUrl = "test.png"
                },
                Images = new List<TravelPlaceImageEntity>(),

            };

            mockTravelPlaceRepository.Setup(x => x.GetTravelPlaceById(travelPlaceId))
                .ReturnsAsync(tarvelPlace);

            var respons = await travelPlaceService.GetTravelPlaceById(travelPlaceId);

            Assert.Equal(respons.Name, tarvelPlace.Name);
        }

        [Fact]
        public async Task GetTravelPlaceById_ShoudReturnNull_WhenDoesNotExist()
        {
            var travelPlaceId = 1;

            mockTravelPlaceRepository.Setup(x => x.GetTravelPlaceById(travelPlaceId))
                .ReturnsAsync(() => null);

            var respons = await travelPlaceService.GetTravelPlaceById(travelPlaceId);

            Assert.Null(respons);
        }

        [Fact]
        public async Task GetTravelPlacesNearby_ShoudReturnAllTravelPlacesNearby()
        {
            var list = new List<TravelPlaceEntity>();
            var latL = 46.7716683;
            var longL = 23.5866262;
            var distance = 20;

            for (int i = 0; i < 3; i++)
            {
                list.Add(new TravelPlaceEntity
                {
                    Id = i,
                    Name = "Test",
                    Lat = 46.7716024,
                    Long = 23.5841182,
                    Description = "Test",
                    Duration = 60,
                    Program = "test.png",
                    CityId = 4,
                    City = new CityEntity
                    {
                        Id = i,
                        Name = "Bucuresti",
                        Lat = 44.33,
                        Long = 44.4,
                        CountryId = 1,
                        ImageUrl = "test.png"
                    },
                    Images = new List<TravelPlaceImageEntity>(),

                });
            }

            mockTravelPlaceRepository.Setup(x => x.GetTravelPlacesNearby(latL,longL))
                .ReturnsAsync(list);

            var listTravelPlaces = await travelPlaceService.GetTravelPlacesNearby(latL, longL,distance);

            Assert.Equal(listTravelPlaces.Count, list.Count);
         
        }

        [Fact]
        public async Task GetTravelPlacesNearbyIfUserLogged_ShoudReturnAllTravelPlacesNearby()
        {
            var list = new List<TravelPlaceEntity>();
            var listFavorites = new List<FavoriteEntity>();
            var latL = 46.7716683;
            var longL = 23.5866262;
            var distance = 20;
            var userId = 1;

            for (int i = 0; i < 3; i++)
            {
                list.Add(new TravelPlaceEntity
                {
                    Id = i,
                    Name = "Test",
                    Lat = 46.7716024,
                    Long = 23.5841182,
                    Description = "Test",
                    Duration = 60,
                    Program = "test.png",
                    CityId = 4,
                    City = new CityEntity
                    {
                        Id = i,
                        Name = "Bucuresti",
                        Lat = 44.33,
                        Long = 44.4,
                        CountryId = 1,
                        ImageUrl = "test.png"
                    },
                    Images = new List<TravelPlaceImageEntity>(),

                });
            }

            for (int i = 0; i < 2; i++)
            {
                listFavorites.Add(new FavoriteEntity
                {
                    TravelPlaceId = i,
                    UserId = userId,
                });
            }

            mockTravelPlaceRepository.Setup(x => x.GetTravelPlacesNearby(latL, longL))
                .ReturnsAsync(list);
            mockFavoriteRepository.Setup(x => x.GetAllFavoriteByUserId(userId))
              .ReturnsAsync(listFavorites);

            var listTravelPlaces = await travelPlaceService.GetTravelPlacesNearbyIfUserLogged(latL, longL, distance,userId);

            Assert.Equal(listTravelPlaces.Count, list.Count);
            Assert.Equal(listTravelPlaces[2].IsFavorite, false);
            Assert.True(listTravelPlaces[0].IsFavorite);

        }
    }
}

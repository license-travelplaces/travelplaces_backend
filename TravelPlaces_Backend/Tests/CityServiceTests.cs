using BusinessLogicLayer.Services.Cities;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class CityServiceTests
    {
        private readonly CityService cityService;
        private readonly Mock<ICityRepository> mockCityRepository = new Mock<ICityRepository>();

        public CityServiceTests()
        {
            this.cityService = new CityService(mockCityRepository.Object);
        }

        [Fact]
        public async Task GetCityById_ShoudReturnCity_WhenCityExist()
        {
            var cityId = 4;
            var cityTest = new CityEntity
            {
                Id = 4,
                Name = "Bucuresti",
                Lat = 34.33,
                Long = 44.4,
                CountryId = 1,
                ImageUrl = "test.png"

            };
            mockCityRepository.Setup(x => x.GetCityById(cityId))
                .ReturnsAsync(cityTest);
            
            var cityResponse= await cityService.GetCityById(cityId);

            Assert.Equal(cityResponse.Id, cityId);
        }

        [Fact]
        public async Task GetCityById_ShoudReturnNull_WhenCityDoesNotExist()
        {
            var cityId = 4;
            mockCityRepository.Setup(x => x.GetCityById(cityId))
                .ReturnsAsync(() => null);
            var cityNull = await cityService.GetCityById(cityId);
            Assert.Equal(cityNull, null);
        }


        [Fact]
        public async Task GetAllCities_ShoudReturnAllCities()
        {
            var list = new List<CityEntity>();

            for (int i = 0; i < 2; i++)
            {
                list.Add(new CityEntity
                {
                    Id = i,
                    Name = "Bucuresti",
                    Lat = 34.33,
                    Long = 44.4,
                    CountryId = 1,
                    ImageUrl = "test.png"

                });
            }

            mockCityRepository.Setup(x => x.GetAllCities())
                .ReturnsAsync(list);

            var allCities = await cityService.GetAllCities();

            Assert.Equal(allCities.Count, list.Count);
        }

        [Fact]
        public async Task GetFirst3Citiess_ShoudReturn3Cities()
        {
            var list = new List<CityEntity>();

            for(int i=0;i<3;i++)
            {
                list.Add(new CityEntity
                {
                    Id = i,
                    Name = "Bucuresti",
                    Lat = 34.33,
                    Long = 44.4,
                    CountryId = 1,
                    ImageUrl = "test.png"

                });
            }

            mockCityRepository.Setup(x => x.GetFirst3Cities())
                .ReturnsAsync(list);

            var listCities = await cityService.GetFirst3Cities();

            Assert.Equal(listCities.Count, 3);
        }

        [Fact]
        public async Task GetSimilarCities_ShoudReturnSimilarCities_WhenExist()
        {
            var list = new List<CityEntity>();
            var inputString1 = "bu";

            for (int i = 0; i < 4; i++)
            {
                list.Add(new CityEntity
                {
                    Id = i,
                    Name = "Bucuresti" + i.ToString(),
                    Lat = 34.33,
                    Long = 44.4,
                    CountryId = 1,
                    ImageUrl = "test.png",
                    Country = new CountryEntity
                    {
                        Id = 1,
                        Name = "Romania",
                    }
                }); ;
            }

            mockCityRepository.Setup(x => x.GetSimilarCities(inputString1))
                .ReturnsAsync(list);

            var listCities = await cityService.GetSimilarCities(inputString1);

            Assert.Equal(listCities.Count, 4);
            Assert.Equal(listCities[0].CountryName, "Romania");
        }

        [Fact]
        public async Task GetSimilarCities_ShoudReturnNull_WhenDoNotExistSimilarCities()
        {
            var list = new List<CityEntity>();
            var inputString1 = "test";

            for (int i = 0; i < 1; i++)
            {
                list.Add(new CityEntity
                {
                    Id = i,
                    Name = "Bucuresti" + i.ToString(),
                    Lat = 34.33,
                    Long = 44.4,
                    CountryId = 1,
                    ImageUrl = "test.png",
                    Country = new CountryEntity
                    {
                        Id = 1,
                        Name = "Romania",
                    }
                }); ;
            }

            mockCityRepository.Setup(x => x.GetSimilarCities(inputString1))
                .ReturnsAsync(() => null);

            var listCities = await cityService.GetSimilarCities(inputString1);

            Assert.Equal(listCities, null);
        }
    }
}

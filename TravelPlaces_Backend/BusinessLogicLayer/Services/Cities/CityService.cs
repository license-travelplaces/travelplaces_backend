﻿using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Cities
{
    public class CityService : ICityService
    {
        private readonly ICityRepository cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            this.cityRepository = cityRepository;
        }

        public async Task<List<CityEntity>> GetAllCities()
        {
            return await cityRepository.GetAllCities();
        }

        public async Task<CityEntity> GetCityById(int cityId)
        {
            return await cityRepository.GetCityById(cityId);
        }

        public async Task<List<CityEntity>> GetFirst3Cities()
        {
            return await cityRepository.GetFirst3Cities();
        }

        public async Task<List<CitySerch>> GetSimilarCities(string inputString)
        {
            var cities = await cityRepository.GetSimilarCities(inputString);
            if (cities == null)
            {
                return null;
            }
            return cities.Select(x =>
            {
                return new CitySerch { Id = x.Id, Name = x.Name, CountryName = x.Country.Name };
            }).ToList();
        }


    }
}

﻿using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Cities
{
    public interface ICityService
    {
        Task<List<CitySerch>> GetSimilarCities(string inputString);
        Task<CityEntity> GetCityById(int cityId);
        Task<List<CityEntity>> GetAllCities();
        Task<List<CityEntity>> GetFirst3Cities();
    }
}

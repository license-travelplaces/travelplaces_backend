﻿using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Favorites
{
    public interface IFavoriteService
    {
        Task CreateFAvoryteAsync(FavoriteDto newFavoryte);
        Task DeleteFavoriteAsync(FavoriteDto newFavoryte);
        Task<List<TravelPlaceDto>> GetAllFavoriteByUserId(int userId);
    }
}

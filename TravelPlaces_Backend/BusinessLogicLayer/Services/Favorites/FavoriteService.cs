﻿using AutoMapper;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Favorites
{
    public class FavoriteService : IFavoriteService
    {
        private readonly IFavoriteRepository favoriteRepository;
        private readonly IMapper _mapper;

        public FavoriteService(IFavoriteRepository favoriteRepository, IMapper mapper)
        {
            this.favoriteRepository = favoriteRepository;
            this._mapper = mapper;
        }
        public async Task CreateFAvoryteAsync(FavoriteDto newFavoryte)
        {
            await favoriteRepository.CreateFavoriteAsync(_mapper.Map<FavoriteDto,FavoriteEntity>(newFavoryte));
        }

        public async Task DeleteFavoriteAsync(FavoriteDto newFavoryte)
        {
            await favoriteRepository.DeleteFavoriteAsync(_mapper.Map<FavoriteDto, FavoriteEntity>(newFavoryte));
        }

        public async Task<List<TravelPlaceDto>> GetAllFavoriteByUserId(int userId)
        {
            var list = await favoriteRepository.GetAllTravelPlacesByUserId(userId);

            if (list == null)
            {
                return null;
            }

            return list.Select(x =>
            {
                var images = x.Images.ToList();
                string image = null;
                var description = "";
                if (images.Count > 0)
                {
                    image = images[0].ImageUrl;
                }
                if (x.Description.Length >= 200)
                {
                    description = x.Description.Substring(0, 200) + " ...";
                } else
                {
                    description = x.Description + "...";
                }
                return new TravelPlaceDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = description,
                    Program = x.Program,
                    Duration = x.Duration,
                    ImageUrl = image,
                    IsFavorite = true
                };
            }).ToList();
        }
    }
}

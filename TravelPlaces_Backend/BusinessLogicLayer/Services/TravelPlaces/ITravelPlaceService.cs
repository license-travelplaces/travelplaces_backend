﻿using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Cities
{
    public interface ITravelPlaceService
    {
        Task<List<TravelPlaceDto>> GetAllTravelPlacesByCityId(int cityId);
        Task<List<TravelPlaceDto>> GetTravelPlacesNearby(double latL, double longL, double distance);
        Task<List<TravelPlaceDto>> GetAllTravelPlacesByCityIdIfUserLogged(int cityId, int userId);
        Task<List<TravelPlaceDto>> GetTravelPlacesNearbyIfUserLogged(double latL, double longL, double distance, int userId);
        Task<TravelPlaceEntity> GetTravelPlaceById(int travelPlaceId);
    }
}

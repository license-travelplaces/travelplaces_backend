﻿using AutoMapper;
using BusinessLogicLayer.Services.Cities;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using DataAcessLayer.Repositories.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.TravelPlaces
{
    public class TravelPlaceService : ITravelPlaceService
    {
        private readonly ITravelPlaceRepository travelPlaceRepository;
        private readonly IFavoriteRepository favoriteRepository;
        private readonly IMapper _mapper;

        public TravelPlaceService(ITravelPlaceRepository travelPlaceRepository, IMapper mapper, IFavoriteRepository favoriteRepository)
        {
            this.travelPlaceRepository = travelPlaceRepository;
            this._mapper = mapper;
            this.favoriteRepository = favoriteRepository;
        }

        public async Task<List<TravelPlaceDto>> GetAllTravelPlacesByCityId(int cityId)
        {
            var list = await travelPlaceRepository.GetAllTravelPlacesByCityId(cityId);
            if (list == null)
            {
                return null;
            }
            return list.Select(x =>
            {
                var images = x.Images.ToList();
                string image = null;
                var description = "";
                if (images.Count > 0)
                {
                    image = images[0].ImageUrl;
                }
                if (x.Description.Length >= 200)
                {
                    description = x.Description.Substring(0, 200) + " ...";
                }
                else
                {
                    description = x.Description + "...";
                }
                return new TravelPlaceDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = description,
                    Program = x.Program,
                    Duration = x.Duration,
                    ImageUrl = image,
                    Lat = x.Lat,
                    Long = x.Long
                };
            }).ToList();
        }

        public async Task<List<TravelPlaceDto>> GetAllTravelPlacesByCityIdIfUserLogged(int cityId, int userId)
        {
            var list = await travelPlaceRepository.GetAllTravelPlacesByCityId(cityId);
            var favorites = await favoriteRepository.GetAllFavoriteByUserId(userId);
            var isFavorite = 1;
        
            return list.Select(x =>
            {
                var images = x.Images.ToList();
                string image = null;
                var description = "";
                if (images.Count > 0)
                {
                    image = images[0].ImageUrl;
                }
                if (x.Description.Length >= 200)
                {
                    description = x.Description.Substring(0, 200) + " ...";
                }
                else
                {
                    description = x.Description + "...";
                }
                if (favorites.Any(y => y.TravelPlaceId == x.Id))
                {
                    isFavorite = 2;
                }
                return new TravelPlaceDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = description,
                    Program = x.Program,
                    Duration = x.Duration,
                    ImageUrl = image,
                    IsFavorite = favorites.Any(y => y.TravelPlaceId == x.Id)
                };
            }).ToList();
        }

        public async Task<TravelPlaceEntity> GetTravelPlaceById(int travelPlaceId)
        {
            return await travelPlaceRepository.GetTravelPlaceById(travelPlaceId);
        }

        public async Task<List<TravelPlaceDto>> GetTravelPlacesNearby(double latL, double longL, double distance)
        {
            var list = await travelPlaceRepository.GetTravelPlacesNearby(latL,longL);
            return list.Select(x =>
            {
                double rlat1 = Math.PI * x.Lat / 180;
                double rlat2 = Math.PI * latL / 180;
                double theta = x.Long - longL;
                double rtheta = Math.PI * theta / 180;
                double dist =
                    Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                    Math.Cos(rlat2) * Math.Cos(rtheta);
                dist = Math.Acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                dist = dist * 1.609344;
                var lala = distance / 1000;
                if (dist <= distance)
                {
                    var images = x.Images.ToList();
                    string image = null;
                    var description = "";
                    if (images.Count > 0)
                    {
                        image = images[0].ImageUrl;
                    }
                    if (x.Description.Length >= 200)
                    {
                        description = x.Description.Substring(0, 200) + " ...";
                    }
                    else
                    {
                        description = x.Description + "...";
                    }
                    return new TravelPlaceDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = description,
                        Program = x.Program,
                        Duration = x.Duration,
                        ImageUrl = image,
                        Distance = Math.Round(dist, 2),
                        CityName = x.City.Name
                    };
                } else
                {
                    return null;
                }
            }).Where(x => x != null).ToList();
        }

        public async Task<List<TravelPlaceDto>> GetTravelPlacesNearbyIfUserLogged(double latL, double longL, double distance, int userId)
        {
            var list = await travelPlaceRepository.GetTravelPlacesNearby(latL, longL);
            var favorites = await favoriteRepository.GetAllFavoriteByUserId(userId);
            var isFavorite = 0;

            return list.Select(x =>
            {
                double rlat1 = Math.PI * x.Lat / 180;
                double rlat2 = Math.PI * latL / 180;
                double theta = x.Long - longL;
                double rtheta = Math.PI * theta / 180;
                double dist =
                    Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                    Math.Cos(rlat2) * Math.Cos(rtheta);
                dist = Math.Acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                dist = dist * 1.609344;
                var lala = distance / 1000;
                if (dist <= distance)
                {
                    var images = x.Images.ToList();
                    string image = null;
                    var description = "";
                    if (images.Count > 0)
                    {
                        image = images[0].ImageUrl;
                    }
                    if (x.Description.Length >= 200)
                    {
                        description = x.Description.Substring(0, 200) + " ...";
                    }
                    else
                    {
                        description = x.Description + "...";
                    }
                    if (favorites.Any(y => y.TravelPlaceId == x.Id))
                    {
                        isFavorite = 1;
                    }
                    return new TravelPlaceDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = description,
                        Program = x.Program,
                        Duration = x.Duration,
                        ImageUrl = image,
                        Distance = Math.Round(dist, 2),
                        CityName = x.City.Name,
                        IsFavorite = favorites.Any(y => y.TravelPlaceId == x.Id)
                    };
                }
                else
                {
                    return null;
                }
            }).Where(x => x != null).ToList();
        }
    }
}

﻿using AutoMapper;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Users
{
    public sealed class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IMapper _mapper; 

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            this.userRepository = userRepository;
            this._mapper = mapper;
        }
        public async Task<UserDto> GetUserByUsernameAndPassword(string username, string password)
        {
            var user = await userRepository.GetUserByUsernameAndPassword(username, password);
            if (user != null)
            {
                return new UserDto
                {
                    Id = user.Id,
                    Username = user.Username,
                    Email = user.Email,
                    Password = null,
                    UsertType = user.UsertType
                };
            }
            else
                return null;
        }
        public async Task CreateUser(UserDto user)
        {
            var newUser = this._mapper.Map<UserEntity>(user);
            await userRepository.CreateUser(newUser);
        }
    }
}

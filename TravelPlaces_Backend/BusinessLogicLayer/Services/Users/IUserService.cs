﻿using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Users
{
    public interface IUserService
    {
        Task<UserDto> GetUserByUsernameAndPassword(string username, string password);
        Task CreateUser(UserDto user);
    }
}

﻿using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Trip
{
    public interface ITripService
    {
        Task<List<PresentationTripDto>> GetTripsByUserId(int userId);
        Task<bool> CreateTrip(TripDto newTrip);
        Task DeleteTripById(int tripId);
        Task<TripEntity> GetTripByTripId(int tripId);
    }
}

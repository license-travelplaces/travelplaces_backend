﻿using AutoMapper;
using BusinessLogicLayer.AI;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services.Trip
{
    public class TripService: ITripService
    {
        private readonly ITravelPlaceRepository travelPlaceRepository;
        private readonly ITripRepository tripRepository;
        private readonly IMapper _mapper;

        public TripService(ITravelPlaceRepository travelPlaceRepository, IMapper mapper, ITripRepository tripRepository)
        {
            this.travelPlaceRepository = travelPlaceRepository;
            this._mapper = mapper;
            this.tripRepository = tripRepository;
        }

        public async Task<bool> CreateTrip(TripDto newTrip)
        {
            var elem = newTrip.HourStart.Split(':');
            var stratHourMin = Int32.Parse(elem[0]) * 60 + Int32.Parse(elem[1]);

            elem = newTrip.HourEnd.Split(':');
            var endHourMin = Int32.Parse(elem[0]) * 60 + Int32.Parse(elem[1]);

            var stratId = newTrip.travelPlacesTrip.FirstOrDefault(x => x.TravelPlaceId == null);
            var utils = new Utils(stratId.Id, newTrip.travelPlacesTrip.Count);

            var ga = new GA(3*newTrip.travelPlacesTrip.Count, utils, newTrip.edges, newTrip.travelPlacesTrip, stratHourMin, endHourMin, newTrip.isStartEnd);
            ga.generatePopulation();
            ga.populationEvaluation();

            var best = ga.BestChromosome();

            for (int i = 0; i < 3 * newTrip.travelPlacesTrip.Count; i++)
            {
                var best2 = ga.BestChromosome();
                if (best.fitness == -1 && best2.fitness > -1)
                {
                    best = best2;
                }
                if (best2.fitness != -1 && best.fitness != -1  && best2.fitness < best.fitness)
                {
                    best = best2;
                }
                ga.oneGenerationElitism();
            }

            if (best.fitness != -1)
            {
                var newTripEntity = new TripEntity
                {
                    Name = newTrip.Name,
                    LatStart = newTrip.Lat,
                    LongStart = newTrip.Long,
                    DateStart = newTrip.Date,
                    EndHour = newTrip.HourEnd,
                    StartHour = newTrip.HourStart,
                    IsStartEnd = newTrip.isStartEnd,
                    UserId = newTrip.UserId,
                    Duration = best.fitness - stratHourMin,
                    StartLocationAddress = newTrip.StartLocationAddress,
                    StartLocationName = newTrip.StartLocationName
                };
                newTripEntity = await tripRepository.CreateTrip(newTripEntity);

                var listEdges = new List<EdgeEntity>();
                for (int i = 1; i < best.formaCromo.Count - 1; i++)
                {
                    var cromo = newTrip.edges.First(x => (x.NodeA == best.formaCromo[i] && x.NodeB == best.formaCromo[i + 1]) || x.NodeB == best.formaCromo[i] && x.NodeA == best.formaCromo[i + 1]);
                    var distance = newTrip.edges.First(x => (x.NodeA == best.formaCromo[i] && x.NodeB == best.formaCromo[i + 1]) || x.NodeB == best.formaCromo[i] && x.NodeA == best.formaCromo[i + 1]).Distance;
                    listEdges.Add(new EdgeEntity
                    {
                        TripId = newTripEntity.Id,
                        StartTravelPlaceId = (int)newTrip.travelPlacesTrip.First(x => x.Id == best.formaCromo[i]).TravelPlaceId,
                        EndTravelPlaceId = (int)newTrip.travelPlacesTrip.First(x => x.Id == best.formaCromo[i + 1]).TravelPlaceId,
                        DText = cromo.DText,
                        DValue = cromo.DValue,
                        TValue = cromo.TValue,
                        TText = cromo.TText,
                        Time = distance
                    });
                }

                await tripRepository.CreateEdges(listEdges);

                return true;

            }
            return false;
        }

        public Task DeleteTripById(int tripId)
        {
            throw new NotImplementedException();
        }

        public async Task<TripEntity> GetTripByTripId(int tripId)
        {
            var trip = await tripRepository.GetTripByTripId(tripId);
            if (trip.Edges != null)
            {
                foreach (var edge in trip.Edges)
                {
                    edge.TravelPlaceStart.Images = edge.TravelPlaceStart.Images.ToList().GetRange(0, 1);
                    edge.TravelPlaceEnd.Images = edge.TravelPlaceEnd.Images.ToList().GetRange(0, 1);
                }
            }

            return trip;
        }

        public async Task<List<PresentationTripDto>> GetTripsByUserId(int userId)
        {
            var trips = await tripRepository.GetTripsByUserId(userId);
            var travelPlacesIds = new List<int>();
            var newList = new List<PresentationTripDto>();

            foreach(var trip in trips)
            {
                foreach (var elem in trip.Edges)
                {
                    travelPlacesIds.Add(elem.StartTravelPlaceId);
                    travelPlacesIds.Add(elem.EndTravelPlaceId);
                }
            }

            travelPlacesIds = travelPlacesIds.Distinct().ToList();
            var listImages = await travelPlaceRepository.GetImagesByTravelPlacesIds(travelPlacesIds);

            foreach (var trip in trips)
            {
                var tp = trip.Edges.Select(x => x.StartTravelPlaceId);
                var tp2 = trip.Edges.Select(x => x.EndTravelPlaceId);
                tp.Concat(tp2).ToList();
                var hours = (int)Math.Ceiling(trip.Duration) / 60;
                var mins = (int)Math.Ceiling(trip.Duration - (hours * 60));
                newList.Add(new PresentationTripDto
                {
                    Id = trip.Id,
                    Name = trip.Name,
                    NumberLocations = trip.Edges.Count + 1,
                    Duration = hours.ToString() + " h " + mins.ToString() + " mins",
                    ImagesUrl = listImages.Where(x => tp.FirstOrDefault(y => y == x.TravelPlaceId) != 0).Select(x => x.ImageUrl).Take(4).ToList()
                });
            }
            return newList;
        }
    }
}

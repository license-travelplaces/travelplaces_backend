﻿using BusinessObjectLayer.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.AI
{
    public class Utils
    {

        private readonly int startId;
        private readonly int numberCities;

        public Utils(int startId, int numberCities)
        {
            this.startId = startId;
            this.numberCities = numberCities;
        }

        public List<int> generateCromosomeForm()
        {
            List<int> shuffled = Enumerable.Range(0, numberCities).ToList<int>();
            Random rng = new Random();
            for (int i = shuffled.Count - 1; i > -1; i--)
            {
                int j = rng.Next(i);
                int tmp = shuffled[i];
                shuffled[i] = shuffled[j];
                shuffled[j] = tmp;
            }
            var poz = shuffled.FindIndex(x => x == startId);
            int tmpp = shuffled[poz];
            shuffled[poz] = shuffled[0];
            shuffled[0] = tmpp;
            return shuffled;
        }  

    }
}

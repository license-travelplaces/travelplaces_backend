﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.AI
{
    public class Chromosome
    {
        private readonly Utils utils;
        public List<int> formaCromo { get; set; }
        public double fitness { get; set; }

        public Chromosome (Utils utils)
        {
            this.utils = utils;
            this.formaCromo = this.utils.generateCromosomeForm();
        }

        public List<int> Crossover(Chromosome c2)
        {
            List<int> shuffled = Enumerable.Range(0, formaCromo.Count).ToList<int>();
            shuffled[0] = formaCromo[0];
            for (int i=1; i<formaCromo.Count; i++)
            {
                shuffled[c2.formaCromo[i] + 1] = formaCromo[i];
            }
            return shuffled;
        }

        public void Mutation()
        {
            Random rnd = new Random();
            var p1 = rnd.Next(1, formaCromo.Count);
            var p2 = rnd.Next(1, formaCromo.Count);

            var r1 = formaCromo[p1];
            var r2 = formaCromo[p2];

            formaCromo[p1] = r2;
            formaCromo[p2] = r1;
        }
    }
}

﻿using BusinessObjectLayer.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.AI
{
    public class GA
    {
        private readonly ICollection<EdgeDto> edges;
        private readonly ICollection<TripTravelPlaceDto> travelPlacesTrip;
        private readonly Utils utils;
        private readonly int nrPopulation;
        private  IList<Chromosome> population;
        private readonly double startHour;
        private readonly double endHour;
        private readonly bool isStartEnd;

        public GA(int nrPopulation, Utils utils, ICollection<EdgeDto> edges, ICollection<TripTravelPlaceDto> travelPlacesTrip, double StartHour, double EndHour, bool isStartEnd)
        {
            this.nrPopulation = nrPopulation;
            this.utils = utils;
            this.edges = edges;
            this.travelPlacesTrip = travelPlacesTrip;
            this.population = new List<Chromosome>();
            this.startHour = StartHour;
            this.endHour = EndHour;
            this.isStartEnd = isStartEnd;
        }

        public void generatePopulation()
        {
            for (int i=0; i<nrPopulation; i++)
            {
                population.Add(new Chromosome(utils));
            }
        }
        public void populationEvaluation()
        {
            for (int i = 0; i < nrPopulation; i++)
            {
                population[i].fitness = this.calculateFitnessForChromosome(population[i]);
            }
        }

        public double calculateFitnessForChromosome(Chromosome cromo)
        {
            var duration = startHour;
            for (int i=0; i< cromo.formaCromo.Count - 1; i++)
            {
                var travelPlaceA = travelPlacesTrip.FirstOrDefault(x => x.Id == cromo.formaCromo[i]);
                var travelPlaceB = travelPlacesTrip.FirstOrDefault(x => x.Id == cromo.formaCromo[i + 1]);
                var timeAB = edges.First(x => (travelPlaceA.Id == x.NodeA && travelPlaceB.Id == x.NodeB) || (travelPlaceA.Id == x.NodeB && travelPlaceB.Id == x.NodeA)).Distance;
                var durationA = travelPlaceA.Duration;
                double closeHourLocationA = -1;
                double openHourLocationB = -1;
                
                if (travelPlaceA.HourClose != null)
                {
                    var elem = travelPlaceA.HourClose.Split(':');
                    closeHourLocationA = Int32.Parse(elem[0]) * 60 + Int32.Parse(elem[1]);
                }
                if (travelPlaceB.HourOpen != null)
                {
                    var elem = travelPlaceB.HourOpen.Split(':');
                    openHourLocationB = Int32.Parse(elem[0]) * 60 + Int32.Parse(elem[1]);
                }

                duration = duration + durationA;
                if (closeHourLocationA!=-1 && duration > closeHourLocationA)
                {
                    return -1;
                }

                duration = duration + timeAB;
                if (openHourLocationB != -1 && duration < openHourLocationB)
                {
                    return -1;
                }
            }

            if (isStartEnd)
            {
                var travelPlaceL = travelPlacesTrip.FirstOrDefault(x => x.Id == cromo.formaCromo[cromo.formaCromo.Count - 1]);
                var travelPlaceS = travelPlacesTrip.FirstOrDefault(x => x.Id == cromo.formaCromo[0]);
                var timeLS = edges.First(x => (travelPlaceL.Id == x.NodeA && travelPlaceS.Id == x.NodeB) || (travelPlaceL.Id == x.NodeB && travelPlaceS.Id == x.NodeA)).Distance;
                var durationL = travelPlaceL.Duration;
                double closeHourLocationL = -1;
                double openHourLocationS = -1;

                if (travelPlaceL.HourClose != null)
                {
                    var elem = travelPlaceL.HourClose.Split(':');
                    closeHourLocationL = Int32.Parse(elem[0]) * 60 + Int32.Parse(elem[1]);
                }
                if (travelPlaceS.HourOpen != null)
                {
                    var elem = travelPlaceS.HourOpen.Split(':');
                    openHourLocationS = Int32.Parse(elem[0]) * 60 + Int32.Parse(elem[1]);
                }

                duration = duration + durationL;
                if (closeHourLocationL != -1 && duration > closeHourLocationL)
                {
                    return -1;
                }

                duration = duration + timeLS;
                if (openHourLocationS != -1 && duration < openHourLocationS)
                {
                    return -1;
                }
            }

            if(duration > endHour)
            {
                return -1;
            }

            return duration;
        }

        public Chromosome BestChromosome()
        {
            var best = population[0];
            for (int i = 1; i < nrPopulation; i++)
            {
                //if (population[i].fitness < best.fitness )
                //{
                //    best = population[i];
                //}
                if (best.fitness == -1 && population[i].fitness > -1)
                {
                    best = population[i];
                }
                if (best.fitness!=-1 && population[i].fitness < best.fitness)
                {
                    best = population[i];
                }
            }
            return best;
        }

        public Chromosome WorstChromosome()
        {
            var wors = population[0];
            for (int i = 1; i < nrPopulation; i++)
            {
                if (population[i].fitness < wors.fitness)
                {
                    wors = population[i];
                }
            }
            return wors;
        }

        public int SelectBestChromosomePosition()
        {
            Random rnd = new Random();
            var p1 = rnd.Next(0, nrPopulation);
            var p2 = rnd.Next(0, nrPopulation);

            if (population[p1].fitness < population[p2].fitness)
            {
                return p1;
            }
            return p2;
        }

        public void oneGenerationElitism()
        {
            IList<Chromosome> newPop = new List<Chromosome>();
            newPop.Add(BestChromosome());
            for (int i = 1; i < nrPopulation; i++)
            {
                var ch1 = population[SelectBestChromosomePosition()];
                var ch2 = population[SelectBestChromosomePosition()];
                //var newCh = ch1.Crossover(ch2);
                //newCh.Mutation();
                //newPop.Add(newCh);
                var newCh = new Chromosome(utils);
                newCh.formaCromo = ch1.Crossover(ch2);
                newCh.Mutation();
                newPop.Add(newCh);
            }

            population = newPop;
            populationEvaluation();
        }
    }
}

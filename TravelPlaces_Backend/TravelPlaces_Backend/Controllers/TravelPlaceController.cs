﻿using BusinessLogicLayer.Services.Cities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelPlaces_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TravelPlaceController : ControllerBase
    {
        private readonly ITravelPlaceService travelPlaceService;

        public TravelPlaceController(ITravelPlaceService travelPlaceService)
        {
            this.travelPlaceService = travelPlaceService;
        }

        [HttpGet]
        [Route("travelPlacesByCity")]
        public async Task<IActionResult> GetAllTravelPlacesByCityId([FromQuery] int cityId)
        {
            var travelPlaces = await travelPlaceService.GetAllTravelPlacesByCityId(cityId);
            if (travelPlaces == null)
            {
                return NoContent();
            }
            return Ok(travelPlaces);
        }

        [HttpGet]
        [Route("travelPlaceNearby")]
        public async Task<IActionResult> GetTravelPlacesNearby([FromQuery] double latL, [FromQuery] double longL, [FromQuery] double distance)
        {
            var travelPlaces = await travelPlaceService.GetTravelPlacesNearby(latL,longL,distance);
            if (travelPlaces == null)
            {
                return NoContent();
            }
            return Ok(travelPlaces);
        }

        [HttpGet]
        [Route("travelPlacesByCityForLoggedUser")]
        public async Task<IActionResult> GetAllTravelPlacesByCityIdForLoggedUser([FromQuery] int cityId, [FromQuery] int userId)
        {
            var travelPlaces = await travelPlaceService.GetAllTravelPlacesByCityIdIfUserLogged(cityId, userId);
            if (travelPlaces == null)
            {
                return NoContent();
            }
            return Ok(travelPlaces);
        }

        [HttpGet]
        [Route("travelPlaceNearbyForLoggedUser")]
        public async Task<IActionResult> GetTravelPlacesNearbyForLoggedUser([FromQuery] double latL, [FromQuery] double longL, [FromQuery] double distance, [FromQuery] int userId)
        {
            var travelPlaces = await travelPlaceService.GetTravelPlacesNearbyIfUserLogged(latL, longL, distance, userId);
            if (travelPlaces == null)
            {
                return NoContent();
            }
            return Ok(travelPlaces);
        }


        [HttpGet]
        [Route("TravelPlaceById")]
        public async Task<IActionResult> GetTravelPlaceById([FromQuery] int travelPlaceId)
        {
            var travelPlace = await travelPlaceService.GetTravelPlaceById(travelPlaceId);
            if (travelPlace == null)
            {
                return NoContent();
            }
            return Ok(travelPlace);
        }
    }
}

﻿using BusinessLogicLayer.Services.Users;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelPlaces_Backend.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    //[EnableCors("AllowAllHeaders")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost]
        [Route("LogIn")]
        public async Task<IActionResult> GetUserByUsernameAndPassword([FromBody] UserDto user)
        {
            var userResponse = await userService.GetUserByUsernameAndPassword(user.Username, user.Password);
            if (userResponse == null)
            {
                return NotFound("User not found!");
            }

            return Ok(userResponse);
        }

        [HttpPost]
        [Route("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody] UserDto user)
        {
            await userService.CreateUser(user);
            if (user == null)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}

﻿using BusinessLogicLayer.AI;
using BusinessLogicLayer.Services.Trip;
using BusinessObjectLayer.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelPlaces_Backend.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class TripController : ControllerBase
    {
        private readonly ITripService tripService;

        public TripController(ITripService tripService)
        {
            this.tripService = tripService;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> CreateFavorite([FromBody] TripDto newTrip)
        {
            
            if (newTrip == null)
            {
                return BadRequest();
            }

            var response = await tripService.CreateTrip(newTrip);

            return Ok(response);
        }
        
        [HttpGet]
        [Route("GetAllTripsByUser")]
        public async Task<IActionResult> GetTripsByUserId([FromQuery] int userId)
        {
            var trips = await tripService.GetTripsByUserId(userId);
            if (trips == null)
            {
                return NoContent();
            }

            return Ok(trips);
        }

        [HttpGet]
        [Route("GetTripByTripId")]
        public async Task<IActionResult> GetTripByTripId([FromQuery] int tripId)
        {
            var trip = await tripService.GetTripByTripId(tripId);
            if (trip == null)
            {
                return NoContent();
            }

            return Ok(trip);
        }



    }
}

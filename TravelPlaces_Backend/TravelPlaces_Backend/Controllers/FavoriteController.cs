﻿using BusinessLogicLayer.Services.Cities;
using BusinessLogicLayer.Services.Favorites;
using BusinessLogicLayer.Services.Users;
using BusinessObjectLayer.Dtos;
using BusinessObjectLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelPlaces_Backend.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteController : ControllerBase
    {
        private readonly IFavoriteService favoriteService;

        public FavoriteController(IFavoriteService favoriteService)
        {
            this.favoriteService = favoriteService;
        }

        [HttpGet]
        [Route("FavoritesByUserId")]
        public async Task<IActionResult> GetAllFavoritesTravelPlacesByUserId([FromQuery] int userId)
        {
            var travePlaces = await favoriteService.GetAllFavoriteByUserId(userId);
            if (travePlaces == null)
            {
                return NoContent();
            }
            return Ok(travePlaces);
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> CreateFavorite([FromBody] FavoriteDto newFavorite)
        {
            if (newFavorite == null)
            {
                ModelState.AddModelError(string.Empty, "Favorite object send from client is null!");
                return BadRequest("Favorite object is null!");
            }
            await favoriteService.CreateFAvoryteAsync(newFavorite);

            return Ok();
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteFavorite([FromQuery] int userId, [FromQuery] int travelPlaceId)
        {
            var favorite = new FavoriteDto
            {
                UserId = userId,
                TravelPlaceId = travelPlaceId
            };
            await favoriteService.DeleteFavoriteAsync(favorite);

            return Ok();
        }

    }
}

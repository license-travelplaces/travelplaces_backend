﻿using BusinessLogicLayer.Services.Cities;
using BusinessLogicLayer.Services.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelPlaces_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ICityService cityService;

        public CityController(ICityService cityService)
        {
            this.cityService = cityService;
        }

        [HttpGet]
        [Route("cities")]
        public async Task<IActionResult> GetSimilarCities([FromQuery] string inputString)
        {
            var cities = await cityService.GetSimilarCities(inputString);
            if (cities == null)
            {
                return NotFound();
            }
            return Ok(cities);
        }

        [HttpGet]
        [Route("city")]
        public async Task<IActionResult> GetCityById([FromQuery] int cityId)
        {
            var city = await cityService.GetCityById(cityId);
            if (city == null)
            {
                return NotFound();
            }
            return Ok(city);
        }

        [HttpGet]
        [Route("allCities")]
        public async Task<IActionResult> GetAllCities()
        {
            var cities = await cityService.GetAllCities();
            if (cities == null)
            {
                return NotFound();
            }
            return Ok(cities);
        }

        [HttpGet]
        [Route("GetFirst3Cities")]
        public async Task<IActionResult> GetFirst3Cities()
        {
            var cities = await cityService.GetFirst3Cities();
            if (cities == null)
            {
                return NotFound();
            }
            return Ok(cities);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAcessLayer.Migrations
{
    public partial class ModofiedTripEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Edges_Trips_TripId",
                table: "Edges");

            migrationBuilder.AddForeignKey(
                name: "FK_Edges_Trips_TripId",
                table: "Edges",
                column: "TripId",
                principalTable: "Trips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Edges_Trips_TripId",
                table: "Edges");

            migrationBuilder.AddForeignKey(
                name: "FK_Edges_Trips_TripId",
                table: "Edges",
                column: "TripId",
                principalTable: "Trips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

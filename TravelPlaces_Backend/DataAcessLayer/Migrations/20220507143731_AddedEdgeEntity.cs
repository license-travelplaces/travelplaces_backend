﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAcessLayer.Migrations
{
    public partial class AddedEdgeEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Edges",
                columns: table => new
                {
                    TripId = table.Column<int>(type: "int", nullable: false),
                    StartTravelPlaceId = table.Column<int>(type: "int", nullable: false),
                    EndTravelPlaceId = table.Column<int>(type: "int", nullable: false),
                    Time = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Edges", x => new { x.TripId, x.StartTravelPlaceId, x.EndTravelPlaceId });
                    table.ForeignKey(
                        name: "FK_Edges_TravelPlaces_EndTravelPlaceId",
                        column: x => x.EndTravelPlaceId,
                        principalTable: "TravelPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Edges_TravelPlaces_StartTravelPlaceId",
                        column: x => x.StartTravelPlaceId,
                        principalTable: "TravelPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Edges_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Edges_EndTravelPlaceId",
                table: "Edges",
                column: "EndTravelPlaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Edges_StartTravelPlaceId",
                table: "Edges",
                column: "StartTravelPlaceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Edges");
        }
    }
}

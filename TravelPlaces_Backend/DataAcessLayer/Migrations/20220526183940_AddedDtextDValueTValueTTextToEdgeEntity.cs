﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAcessLayer.Migrations
{
    public partial class AddedDtextDValueTValueTTextToEdgeEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Time",
                table: "Edges",
                newName: "TValue");

            migrationBuilder.AddColumn<string>(
                name: "DText",
                table: "Edges",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "DValue",
                table: "Edges",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "TText",
                table: "Edges",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DText",
                table: "Edges");

            migrationBuilder.DropColumn(
                name: "DValue",
                table: "Edges");

            migrationBuilder.DropColumn(
                name: "TText",
                table: "Edges");

            migrationBuilder.RenameColumn(
                name: "TValue",
                table: "Edges",
                newName: "Time");
        }
    }
}

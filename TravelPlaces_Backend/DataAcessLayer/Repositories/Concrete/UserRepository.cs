﻿using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Concrete
{
    public sealed class UserRepository : IUserRepository
    {
        private readonly Context dbContext;

        public UserRepository(Context dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<UserEntity> GetUserByUsernameAndPassword(string username, string password)
        {
            return dbContext.Users.AsNoTracking()
                .FirstOrDefaultAsync(user => user.Username == username && user.Password == password);
        }

        public async Task CreateUser(UserEntity user)
        {
            dbContext.Users.Add(user);
            await dbContext.SaveChangesAsync();
        }
    }
}

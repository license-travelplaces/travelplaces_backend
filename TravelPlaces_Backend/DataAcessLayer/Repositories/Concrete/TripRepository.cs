﻿using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Concrete
{
    public class TripRepository : ITripRepository
    {
        private readonly Context dbContext;

        public TripRepository(Context dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task CreateEdges(List<EdgeEntity> listedges)
        {
            foreach(var elem in listedges)
            {
                dbContext.Edges.Add(elem);
            }
            await dbContext.SaveChangesAsync();
        }

        public async Task<TripEntity> CreateTrip(TripEntity newTrip)
        {
            dbContext.Trips.Add(newTrip);
            await dbContext.SaveChangesAsync();

            return newTrip;
        }

        public Task DeleteTripById(int tripId)
        {
            throw new NotImplementedException();
        }

        public Task<TripEntity> GetTripByTripId(int tripId)
        {
            return dbContext.Trips
                .Where(x => x.Id == tripId)
                .Include(x => x.Edges)
                    .ThenInclude(x => x.TravelPlaceStart)
                        .ThenInclude(l => l.Images)
                .Include(x => x.Edges)
                    .ThenInclude(x => x.TravelPlaceEnd)
                        .ThenInclude(l => l.Images)
                .FirstOrDefaultAsync(x => x.Id == tripId);
        }

        public Task<List<TripEntity>> GetTripsByUserId(int userId)
        {
            return dbContext.Trips
                .Where(x => x.UserId == userId)
                .Include(x => x.Edges)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}

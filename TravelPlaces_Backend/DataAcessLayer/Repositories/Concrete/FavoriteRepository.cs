﻿using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Concrete
{
    public class FavoriteRepository : IFavoriteRepository
    {
        private readonly Context dbContext;

        public FavoriteRepository(Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public async Task CreateFavoriteAsync(FavoriteEntity newFavoryte)
        {
            dbContext.Favorites.Add(newFavoryte);
            await dbContext.SaveChangesAsync();
        }

        public async Task DeleteFavoriteAsync(FavoriteEntity newFavoryte)
        {
            dbContext.Favorites.Remove(newFavoryte);
            await dbContext.SaveChangesAsync();
        }

        public Task<List<FavoriteEntity>> GetAllFavoriteByUserId(int userId)
        {
            return dbContext.Favorites
                .Where(x => x.UserId == userId)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<List<TravelPlaceEntity>> GetAllTravelPlacesByUserId(int userId)
        {
            return dbContext.Favorites
                .Where(x => x.UserId == userId)
                .Include(x => x.TravelPlace)
                    .ThenInclude(x => x.Images)
                .Select(x => x.TravelPlace)
                .AsNoTracking()
                .ToListAsync();    
        }
    }
}

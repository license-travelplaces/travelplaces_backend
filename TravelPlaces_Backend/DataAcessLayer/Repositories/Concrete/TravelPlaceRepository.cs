﻿using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Concrete
{
    public class TravelPlaceRepository : ITravelPlaceRepository
    {
        private readonly Context dbContext;

        public TravelPlaceRepository(Context dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<List<TravelPlaceEntity>> GetAllTravelPlacesByCityId(int cityId)
        {
            return dbContext.TravelPlaces
                .Where(x => x.CityId == cityId)
                .Include(x => x.Images)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<List<TravelPlaceImageEntity>> GetImagesByTravelPlacesIds(List<int> travalPlacesIds)
        {
            return dbContext.TravelPlaceImages
                .Where(x => travalPlacesIds.Any(y => y == x.TravelPlaceId))
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<TravelPlaceEntity> GetTravelPlaceById(int travelPlaceId)
        {
            return dbContext.TravelPlaces.AsNoTracking()
                .Include(x => x.Images)
                .Include(x => x.City)
                .FirstOrDefaultAsync(x => x.Id == travelPlaceId);
        }

        public Task<List<TravelPlaceEntity>> GetTravelPlacesNearby(double latL, double longL)
        {
            return dbContext.TravelPlaces
                .Where(x => Math.Abs(x.Lat - latL) <= 0.5 && Math.Abs(x.Long - longL) <= 0.5)
                .Include(x => x.Images)
                .Include(x => x.City)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}

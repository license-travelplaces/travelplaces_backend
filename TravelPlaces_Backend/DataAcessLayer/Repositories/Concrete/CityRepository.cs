﻿using BusinessObjectLayer.Entities;
using DataAcessLayer.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Concrete
{
    public class CityRepository : ICityRepository
    {
        private readonly Context dbContext;

        public CityRepository(Context dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<List<CityEntity>> GetAllCities()
        {
            return dbContext.Cities
                .Include(x => x.Country)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<CityEntity> GetCityById(int cityId)
        {
            return dbContext.Cities
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == cityId);
        }

        public Task<List<CityEntity>> GetFirst3Cities()
        {
            return dbContext.Cities
                .Take(3)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<List<CityEntity>> GetSimilarCities(string inputString)
        {
            return dbContext.Cities
                .Where(x => x.Name.Contains(inputString))
                .Include(x => x.Country)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}

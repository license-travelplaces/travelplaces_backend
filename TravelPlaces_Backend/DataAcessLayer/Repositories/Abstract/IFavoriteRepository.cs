﻿using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Abstract
{
    public interface IFavoriteRepository
    {
        Task CreateFavoriteAsync(FavoriteEntity newFavoryte);
        Task DeleteFavoriteAsync(FavoriteEntity newFavoryte);
        Task<List<TravelPlaceEntity>> GetAllTravelPlacesByUserId(int userId);
        Task<List<FavoriteEntity>> GetAllFavoriteByUserId(int userId);
    }
}

﻿using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Abstract
{
    public interface ITravelPlaceRepository
    {
        Task<List<TravelPlaceEntity>> GetAllTravelPlacesByCityId(int cityId);
        Task<List<TravelPlaceEntity>> GetTravelPlacesNearby(double latL, double longL);
        Task<TravelPlaceEntity> GetTravelPlaceById(int travelPlaceId);

        Task<List<TravelPlaceImageEntity>> GetImagesByTravelPlacesIds(List<int> travalPlacesIds);
    }
}

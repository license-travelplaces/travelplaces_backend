﻿using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Abstract
{
    public interface ITripRepository
    {
        Task<List<TripEntity>> GetTripsByUserId(int userId);
        Task<TripEntity> CreateTrip(TripEntity newTrip);
        Task DeleteTripById(int tripId);
        Task<TripEntity> GetTripByTripId(int tripId);
        Task CreateEdges(List<EdgeEntity> listedges);
    }
}

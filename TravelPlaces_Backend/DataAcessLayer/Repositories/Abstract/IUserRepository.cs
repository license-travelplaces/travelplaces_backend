﻿using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Abstract
{
    public interface IUserRepository
    {
        Task<UserEntity> GetUserByUsernameAndPassword(string username, string password);
        Task CreateUser(UserEntity user);
    }
}

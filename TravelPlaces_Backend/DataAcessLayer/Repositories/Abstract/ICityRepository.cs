﻿using BusinessObjectLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.Abstract
{
    public interface ICityRepository
    {
        Task<CityEntity> GetCityById(int cityId);
        Task<List<CityEntity>> GetSimilarCities(string inputString);
        Task<List<CityEntity>> GetAllCities();
        Task<List<CityEntity>> GetFirst3Cities();
    }
}

﻿using BusinessObjectLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace DataAcessLayer
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<UserEntity> Users { get; set; }

        public DbSet<CountryEntity> Countries { get; set; }

        public DbSet<CityEntity> Cities { get; set; }

        public DbSet<TravelPlaceEntity> TravelPlaces { get; set; }

        public DbSet<TravelPlaceImageEntity> TravelPlaceImages { get; set; }

        public DbSet<FavoriteEntity> Favorites { get; set; }

        public DbSet<TripEntity> Trips { get; set; }

        public DbSet<EdgeEntity> Edges { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FavoriteEntity>()
                .HasKey(nameof(FavoriteEntity.UserId), nameof(FavoriteEntity.TravelPlaceId));

            modelBuilder.Entity<EdgeEntity>()
                .HasKey(nameof(EdgeEntity.TripId), nameof(EdgeEntity.StartTravelPlaceId), nameof(EdgeEntity.EndTravelPlaceId));

            modelBuilder.Entity<EdgeEntity>()
               .HasOne(typeof(TravelPlaceEntity), "TravelPlaceStart")
               .WithMany()
               .HasForeignKey("StartTravelPlaceId")
               .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<EdgeEntity>()
               .HasOne(typeof(TravelPlaceEntity), "TravelPlaceEnd")
               .WithMany()
               .HasForeignKey("EndTravelPlaceId")
               .OnDelete(DeleteBehavior.Restrict);
           
        }

    }
}
